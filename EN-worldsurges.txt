Worldsurges

Self-image and confession of the natives


§1.1
First the Creator drew the land from the water. This is how the earth was obtained.

§1.2
Then HE let the land bring forth man.

§1.3
Man as a divine being created by God is not a thing, but stands above things as the crown of creation.

§1.4
Man was created immortal in the image of God and was subject only to God’s laws.

§2.1
It was only because man voluntarily left the legal circle of paradise due to deception that the enemy of mankind, the fascist, Satan, the old serpent, the Nachash, consequently managed to persuade man to take up this offer of founding other legal circles.

§2.2
Because of this, he lost his immortality. Scripture calls this consequence of failing to keep the order of God “becoming mortal”.

§2.3
Enoch’s founding of a city from Cain’s bloodline was the proclamation of the first anti-divine legal circle. Further city foundations followed. The result was war and conquest. Since God is a God of peace, this reveals the difference between the divine and those satanic legal circles.

§3.1
Nationality in Christ is re-creation of natural law without regard to person or worldly nationality. We respect the diversity of nations and cultures and these are protected by God’s natural law. A worldly amalgamation and bastardisation of nations annihilates, threatens and destroys all cultural, spiritual, mental, genetic, ethical and moral identity.

§3.2
It is we who have the priestly mandate to bring people back into peace with God’s order.

§3.3
Hence our request to conclude peace treaties with all the world, irrespective of their legal system. As far as it is up to us, we are at peace with all people.

§4.1
The legal circles of the serpent were proclaimed and the flooding of humanity was sealed as the coming judgement of God on the fallen world.

§4.2
The bottom of the sea, also called the abyss, is the grounds of Satan’s laws – the soil specifically as garden was the basis of the enactment of divine law. Here the law of the sea stands against the divinely created natural law.

§4.3
Our soil was created by God as the soil of the earth and not as the ground of the sea, which is evident in the history of creation, the separation of water and land.

§4.4
The story of the Flood is therefore also to be seen as an allegory of the fact that the marine law of the sea ground was extended over the earth, which is therefore also called ground today. This is reflected in words like “basic law” (germ. „Grundrecht“) and “base tax” (germ. „Grundsteuer“). This is the consequence and legal establishment of the taking of land.

§5.1
The priest Melchisedec, who has no pedigree, is awakened and not born. We too are in Christ, the priest after the order of Melchisedec, a new creation and not born of the world system, and not of this world. This happened through the acceptance of God’s judgement and the awakening of the Spirit of Christ in the old creature that died to the world.

§5.2
By divine law, man is the crown of creation and cannot be a thing unless he is dehumanised. At present, in Germany and worldwide, man is treated as a thing and not as an ensouled, divine being.

§6
We profess the divine circle of law, which in Christ works in us newly created according to the order of Melchisedec. Through the example of Jesus in Christ, we are rightfully a new creation. For the old has already passed away and we are restored to the divine order of creation.