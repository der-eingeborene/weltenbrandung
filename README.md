# Weltenbrandung

## Selbstverständnis und Bekenntnis der Eingeborenen

Zuerst schöpfte der Schöpfer das Land aus dem Wasser. So wurde die Erde gewonnen.

Dann ließ ER das Land den Menschen hervorbringen.

Der Mensch als göttliches Wesen von Gott geschaffen ist keine Sache, sondern er steht über den Din­gen als Krone der Schöpfung.

…

### Deine Mitwirkung an der Ausformulierung in der entsprechenden Geisteshaltung ist ausdrücklich erwünscht!

### Telegram:
https://t.me/weltenbrandung

### Web:
https://weltenbrandung.de